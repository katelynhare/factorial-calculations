import java.util.Scanner;

public class Factorial
{
  public static void main (String args[])
  {
    Scanner scan = new Scanner ( System.in );
    int number;
    int factorial = 1;
    
    System.out.println("Enter a number: ");
    number = scan.nextInt();
    int count = number;
    
    while (count > 0)
    {
      factorial = factorial * number;
      count--;
    }
    
    if (number <= 0)
    {
      System.out.println("Please enter a positive number that is not zero.");
    }
    else
    {
      System.out.println("The factorial of "+number+" is "+factorial);
     }
  }
}